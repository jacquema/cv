---
permalink: /research/
title: research
description: research interests
---

My research project is concerned with Computer Music and Digital Humanities, 
more specifically with the processing of structured written music data (*digital music scores*), in particular the problems of:
* Automatic Music **Transcription**
* **Melodic Similarity** computation
* Digital Music Score and Corpora **Analysis** (Digital Musicology)
* **Information Retrieval** in collections of digital music scores

The agenda for tackling these problems is to apply and contribute to 
research on formal methods and tools issued from the following fundamental domains:
* Theory of Automata and **Tree Automata**, **weighted** and unweighted, 
* **Edit Distances** between strings and trees, 
* Logic for Computer Science, 
* Term Rewriting Systems.

Formerly, I have worked on these formal methods and their application to the verification of systems and software:
* Interactive Music Systems, Real-Time Testing and Computer-Aided Composition at **[Ircam](https://www.ircam.fr)**, Paris (team [Mutant](http://repmus.ircam.fr/mutant)),
* Verification of Web data management systems and Computer Security at **[LSV](http://www.lsv.fr)/ENS-Cachan**, 
* Automated Deduction at **[Inria Nancy](http://www.loria.fr)**, **[MPI-I](https://www.mpi-inf.mpg.de)** Saarbrücken, **[SRI International](http://www.csl.sri.com)**, Stanford,
* development of secure embedded software components for smartcards and payment terminals at the company _Trusted Logic_.

