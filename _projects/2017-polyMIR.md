---
name: PolyMIR 
network: emergence Sorbonne Universités
duration: 2017 &mdash; 2018
fullname: Music Information Retrieval in Polyphonic Corpora
---

18 months exploratory project between IReMus, Ircam/STMS and CNAM-CEDRIC.

