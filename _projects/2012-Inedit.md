---
name: Inedit 
link: "http://inedit.ircam.fr"
network: ANR ConInt
duration: 2012 &mdash; 2015
---

Project between Ircam (UMR 9912 STMS), LaBRI (University Bordeaux) and Grame (Centre National de Création Musicale, Lyon) on authoring of time and interaction in real-time computer music.   

